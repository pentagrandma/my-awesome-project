package com.project.project.service;


import com.project.project.domain.Person;
import com.project.project.entity.PersonEntity;
import com.project.project.repository.PersonRepository;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class PersonService {

    private PersonRepository personRepository;

    public PersonService(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }


    public void insertOrUpdate(Person person) {
        personRepository.save(PersonEntity.from(person));
    }

    public void delete(Person person) {
        personRepository.delete(PersonEntity.from(person));
    }

    public List<Person> retrieveAll(){
        return personRepository
                .findAll()
                .stream()
                .map( (personEntity) ->
                        new Person
                            .Builder()
                            .withDesc(personEntity.getDesc())
                            .withId(personEntity.getId())
                            .build()
                ).collect(Collectors.toList());
    }

    public Person retrieveOne(UUID id){
        var person = personRepository.findById(id).orElseThrow();
        return new Person
                .Builder()
                .withId(person.getId())
                .withDesc(person.getDesc())
                .build();
    }

}
