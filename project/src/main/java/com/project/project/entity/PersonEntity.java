package com.project.project.entity;


import com.project.project.domain.Person;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name="person")
public final class PersonEntity {

    @Id
    private UUID id;
    @Column(name="desc")
    private String desc;

    public PersonEntity(){}

    private PersonEntity(Builder builder) {
        setId(builder.id);
        setDesc(builder.desc);
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public static PersonEntity from(Person person){
        return new Builder()
                .withId(person.getId())
                .withDesc(person.getDesc())
                .build();
    }

    public static final class Builder {
        private UUID id;
        private String desc;

        public Builder() {
        }

        public Builder withId(UUID val) {
            id = val;
            return this;
        }

        public Builder withDesc(String val) {
            desc = val;
            return this;
        }

        public PersonEntity build() {
            return new PersonEntity(this);
        }
    }

}
