package com.project.project.domain;


import java.util.UUID;

public class Person {
    private UUID id;
    private String desc;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Person() {}

    private Person(Builder builder) {
        id = builder.id;
        desc = builder.desc;
    }

    public static final class Builder {
        private UUID id;
        private String desc;

        public Builder() {
        }

        public Builder withId(UUID val) {
            id = val;
            return this;
        }

        public Builder withDesc(String val) {
            desc = val;
            return this;
        }

        public Person build() {
            return new Person(this);
        }
    }
}
