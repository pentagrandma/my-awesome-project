package com.project.project.entity;

import com.project.project.domain.Person;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PersonEntityTest {

    UUID id;
    String desc;
    @BeforeEach
    void setUp(){
        desc = "desc";
        id = UUID.randomUUID();
    }

    @Test
    void EntityCreationTest(){

        var personEntity = new PersonEntity();
        personEntity.setDesc(desc);
        personEntity.setId(id);

        assertEquals(desc,personEntity.getDesc());
        assertEquals(id,personEntity.getId());
    }
    @Test
    void EntityBuildingTest(){

        var personEntity = new PersonEntity
                .Builder()
                .withDesc(desc)
                .withId(id)
                .build();
        assertEquals(desc,personEntity.getDesc());
        assertEquals(id,personEntity.getId());
    }
    @Test
    void EntityFromDomainCreationTest(){
        var personDomain = new Person
                .Builder()
                .withDesc(desc)
                .withId(id)
                .build();
        var personEntity = PersonEntity.from(personDomain);

        assertEquals(desc,personEntity.getDesc());
        assertEquals(id,personEntity.getId());

    }
}
