package com.project.project.Data;

import com.project.project.entity.PersonEntity;
import com.project.project.repository.PersonRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;
import java.util.UUID;

import static org.junit.Assert.assertEquals;


@ExtendWith(SpringExtension.class)
@DataJpaTest

public class PersonDataTest {

    @Autowired
    PersonRepository personRepository;

    UUID id;
    PersonEntity personEntity;

    @BeforeEach
    void setUp(){
        id = UUID.fromString("7593c86f-cccb-4c13-9784-72aecb48abef");
        personEntity = new PersonEntity
                .Builder()
                .withId(id)
                .withDesc("desc")
                .build();
        personRepository.deleteAll();
        personRepository.save(personEntity);
    }
    @Test
    void SaveTest(){

        personEntity.setDesc("new desc");

        personRepository.save(personEntity);
        var recoveredPerson = personRepository.findById(id);
        assertEquals("new desc",recoveredPerson.get().getDesc());

    }
    @Test
    void FindAllTest(){
        var personEntityTwo = new PersonEntity.Builder()
                .withId(UUID.fromString("1111c86f-cccb-4c13-9784-72aecb48abef"))
                .withDesc("descTwo")
                .build();
        var personEntityThree = new PersonEntity.Builder()
                .withId(UUID.fromString("2222c86f-cccb-4c13-9784-72aecb48abef"))
                .withDesc("descThree")
                .build();
         personRepository.saveAll(List.of(personEntity,personEntityTwo,personEntityThree));

         var personEntityList = personRepository.findAll();

        assertEquals(personEntityList.get(0).getId(),personEntity.getId());
        assertEquals(personEntityList.get(1).getId(),personEntityTwo.getId());
        assertEquals(personEntityList.get(2).getId(),personEntityThree.getId());
    }

    @Test
    void deleteTest(){
        assert(personRepository.findAll().size() == 1);
        personRepository.deleteById(personEntity.getId());
        assert(personRepository.findAll().size() == 0);
    }
}
