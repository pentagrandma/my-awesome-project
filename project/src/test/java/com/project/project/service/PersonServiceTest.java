package com.project.project.service;

import com.project.project.domain.Person;
import com.project.project.entity.PersonEntity;
import com.project.project.repository.PersonRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class PersonServiceTest {

    PersonRepository personRepositoryMock;
    Person person;
    PersonService personService;


    @BeforeEach
    void setUp(){
        personRepositoryMock = Mockito.mock(PersonRepository.class);
        personService = new PersonService(personRepositoryMock);
        UUID id = UUID.fromString("deca23a7-f9ef-4617-af7f-cee81c197538");
        person = new Person.Builder()
                .withDesc("Description")
                .withId(id)
                .build();
    }

    @Test
    void insertPerson(){
        ArgumentCaptor<PersonEntity> argumentCaptor = ArgumentCaptor.forClass(PersonEntity.class);

        personService.insertOrUpdate(person);

        verify(personRepositoryMock).save(argumentCaptor.capture());
        Mockito.verifyNoMoreInteractions(personRepositoryMock);

        assertEquals(person.getId(),argumentCaptor.getValue().getId());
        assertEquals(person.getDesc(),argumentCaptor.getValue().getDesc());
    }

    @Test
    void deletePerson(){
        ArgumentCaptor<PersonEntity> argumentCaptor = ArgumentCaptor.forClass(PersonEntity.class);

        personService.delete(person);

        verify(personRepositoryMock).delete(argumentCaptor.capture());
        Mockito.verifyNoMoreInteractions(personRepositoryMock);

        assertEquals(person.getId(),argumentCaptor.getValue().getId());
        assertEquals(person.getDesc(),argumentCaptor.getValue().getDesc());

    }

    @Test
    void RetrieveAllPersons(){
        when(personRepositoryMock.findAll()).thenReturn(Collections.emptyList());
        var personList = personService.retrieveAll();

        verify(personRepositoryMock).findAll();
        verifyNoMoreInteractions(personRepositoryMock);

        assertEquals(personList,Collections.emptyList());
    }
    @Test
    void RetrieveAllPersonsMapping(){
        when(personRepositoryMock.findAll()).thenReturn(List.of(PersonEntity.from(person)));
        var personList = personService.retrieveAll();

        verify(personRepositoryMock).findAll();
        verifyNoMoreInteractions(personRepositoryMock);

        assertEquals(personList.get(0).getId(),person.getId());
        assertEquals(personList.get(0).getDesc(),person.getDesc());
    }
    @Test
    void retrieveOnePerson(){

        ArgumentCaptor<UUID> argumentCaptor = ArgumentCaptor.forClass(UUID.class);
        when(personRepositoryMock.findById(any()))
            .thenReturn(Optional.of(PersonEntity.from(person)));

        personService.retrieveOne(person.getId());
        verify(personRepositoryMock).findById(argumentCaptor.capture());

        assertEquals(person.getId(),argumentCaptor.getValue());

        verifyNoMoreInteractions(personRepositoryMock);
    }
}
