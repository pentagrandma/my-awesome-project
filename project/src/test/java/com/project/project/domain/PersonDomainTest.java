package com.project.project.domain;


import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PersonDomainTest {

    @Test
    void personBuildingTest(){
        var id = UUID.randomUUID();
        var desc = "Description";

        var person = new Person
                .Builder()
                .withId(id)
                .withDesc(desc)
                .build();
        assertEquals(id,person.getId());
        assertEquals(desc,person.getDesc());
    }
    @Test
    void personCreationTest(){
        var id = UUID.randomUUID();
        var desc = "Description";

        var person = new Person();
        person.setDesc(desc);
        person.setId(id);
        assertEquals(id,person.getId());
        assertEquals(desc,person.getDesc());
    }
}
